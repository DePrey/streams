(function() {
    var siteDomain = 'dev-sandbox.npr.org';
    var gdprEndpoint = 'https://identity.api.npr.org/v2/gdpr';
    var choiceEndpoint = 'https://choice.npr.org/'

    console.log('Initial cookie: ' + document.cookie);

    if (document.cookie.indexOf('gdpr=true') === -1) {
        if (document.referrer === choiceEndpoint + '?origin=' + location.href) {
            console.log('Registering GDPR consent');
            document.cookie = 'gdpr=true;domain=' + siteDomain + ';path=/';
            console.log('Updated cookie: ' + document.cookie);
        } else {
            var request = new XMLHttpRequest();
            request.open('GET', gdprEndpoint, true);
            request.addEventListener('load', function (e) {
                console.log('User is outside of EU');
                document.cookie = 'gdpr=true;domain=' + siteDomain + ';path=/';
                console.log('Updated cookie: ' + document.cookie);
            });
            request.addEventListener('error', function (e) {
                console.log('EU user has not yet accepted tracking');
                window.location.href = gdprEndpoint + '?origin=' + location.href;
            });
            request.send();
        }
    }
})();