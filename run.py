# run.py
from flask import Flask, jsonify, render_template
import datetime as dt
import sys, pathlib
directory = pathlib.Path(__file__).parent
sys.path.insert(1, str(directory))

#from data.logic import functionName

meta = {
    'title': 'project name',
    'logo': 'https://media.npr.org/chrome_svg/npr-logo-color.svg',
    'primaryImg': 'https://media.npr.org/chrome_svg/npr-logo-color.svg',
    'version': '0.0.0',
    'pyVersion': str(sys.version_info.major)+'.'+str(sys.version_info.minor)+' on '+sys.prefix,
    'year': dt.datetime.today().strftime('%Y')
       }

app = Flask(__name__)
application = app #wsgi doesn't like abbreviations

@app.route('/')
def home():
    return render_template('index.html',
                           meta = meta)

'''
@app.route('/api', methods=['GET'])
def api():
    return jsonify(functionName)
'''

@app.errorhandler(Exception)
def exception_handler(error):
    return repr(error)
